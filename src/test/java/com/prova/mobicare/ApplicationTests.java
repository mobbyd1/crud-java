package com.prova.mobicare;

import com.prova.mobicare.controller.DepartmentController;
import com.prova.mobicare.controller.EmployeeController;
import com.prova.mobicare.dlo.DepartmentDLO;
import com.prova.mobicare.dlo.EmployeeDLO;
import com.prova.mobicare.model.Department;
import com.prova.mobicare.model.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationTests {

	@MockBean
	private EmployeeDLO employeeDLO;

	@MockBean
	private DepartmentDLO departmentDLO;

	@Autowired
	private MockMvc mvc;


	@Test
	public void testGetEmployeesOK() throws Exception {
		mvc.perform(
				MockMvcRequestBuilders.get("/getAllEmployees"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(0)));
	}

	@Test
	public void testGetEmployeesOK2() throws Exception {
		final List<Employee> employees = new ArrayList<>();

		final Department department = new Department("d1");

		final Employee e1 = new Employee("e1");
		e1.setId(Long.valueOf("1"));
		e1.setDepartment(department);

		final Employee e2 = new Employee("e2");
		e2.setId(Long.valueOf("2"));
		e2.setDepartment(department);

		employees.add( e1 );
		employees.add( e2 );

		when(employeeDLO.getAll()).thenReturn(employees);

		mvc.perform(
				MockMvcRequestBuilders.get("/getAllEmployees"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].id", is("1")))
				.andExpect(jsonPath("$[0].name", is("e1")))
				.andExpect(jsonPath("$[0].department", is("d1")))
				.andExpect(jsonPath("$[1].id", is("2")))
				.andExpect(jsonPath("$[1].name", is("e2")))
				.andExpect(jsonPath("$[1].department", is("d1")));
	}

	@Test
	public void testSaveEmployeeOK() throws Exception {
		final String url = "/saveEmployee?nome=e1&departamento=1";
		mvc.perform(
				MockMvcRequestBuilders.get(url))
				.andExpect(status().isOk());
	}

	@Test
	public void testSaveEmployee400() throws Exception {
		final String url = "/saveEmployee?nome=e1";
		mvc.perform(
				MockMvcRequestBuilders.get(url))
				.andExpect(status().isBadRequest());

		final String url2 = "/saveEmployee?departamento=1";
		mvc.perform(
				MockMvcRequestBuilders.get(url2))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testEditEmployeeOK() throws Exception {
		final String url = "/editEmployee?id=1&nome=e1&departamento=1";
		mvc.perform(
				MockMvcRequestBuilders.get(url))
				.andExpect(status().isOk());
	}

	@Test
	public void testEditEmployee400() throws Exception {
		final String url = "/editEmployee?nome=e1&departamento=1";
		mvc.perform(
				MockMvcRequestBuilders.get(url))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testRemoveEmployeeOK() throws Exception {
		final String url = "/removeEmployee?id=1";
		mvc.perform(
				MockMvcRequestBuilders.get(url))
				.andExpect(status().isOk());
	}

	@Test
	public void testRemoveEmployee400() throws Exception {
		final String url = "/removeEmployee";
		mvc.perform(
				MockMvcRequestBuilders.get(url))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testGetDepartmentsOK() throws Exception {
		mvc.perform(
				MockMvcRequestBuilders.get("/getAllDepartments"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(0)));
	}

	@Test
	public void testGetDepartmentsOK2() throws Exception {

		final List<Department> departments = new ArrayList<>();

		final Department department = new Department("d1");
		department.setId(Long.valueOf(1));

		final Department department2 = new Department("d2");
		department2.setId(Long.valueOf(2));

		final Department department3 = new Department("d3");
		department3.setId(Long.valueOf(3));

		final Employee e1 = new Employee("e1");
		e1.setId(Long.valueOf("1"));
		e1.setDepartment(department);

		final Employee e2 = new Employee("e2");
		e2.setId(Long.valueOf("2"));
		e2.setDepartment(department);

		department.addEmployee( e1 );
		department.addEmployee( e2 );

		departments.add( department );
		departments.add( department2 );
		departments.add( department3 );

		when(departmentDLO.getAll()).thenReturn(departments);

		mvc.perform(
				MockMvcRequestBuilders.get("/getAllDepartments"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(3)))
				.andExpect(jsonPath("$[0].id", is("1")))
				.andExpect(jsonPath("$[0].name", is("d1")))
				.andExpect(jsonPath("$[0].numberOfEmployees", is("2")))
				.andExpect(jsonPath("$[1].id", is("2")))
				.andExpect(jsonPath("$[1].name", is("d2")))
				.andExpect(jsonPath("$[1].numberOfEmployees", is("0")))
				.andExpect(jsonPath("$[2].id", is("3")))
				.andExpect(jsonPath("$[2].name", is("d3")))
				.andExpect(jsonPath("$[2].numberOfEmployees", is("0")));

	}

	@Test
	public void testGetEmptyDepartments() throws Exception {

		final List<Department> departments = new ArrayList<>();

		final Department department2 = new Department("d2");
		department2.setId(Long.valueOf(2));

		final Department department3 = new Department("d3");
		department3.setId(Long.valueOf(3));

		departments.add( department2 );
		departments.add( department3 );

		when(departmentDLO.getEmptyDepartments()).thenReturn(departments);

		mvc.perform(
				MockMvcRequestBuilders.get("/getEmptyDepartments"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].id", is("2")))
				.andExpect(jsonPath("$[0].name", is("d2")))
				.andExpect(jsonPath("$[0].numberOfEmployees", is("0")))
				.andExpect(jsonPath("$[1].id", is("3")))
				.andExpect(jsonPath("$[1].name", is("d3")))
				.andExpect(jsonPath("$[1].numberOfEmployees", is("0")));

	}

	@Test
	public void testRemoveDepartmentOK() throws Exception {
		final String url = "/removeDepartment?id=1";
		mvc.perform(
				MockMvcRequestBuilders.get(url))
				.andExpect(status().isOk());
	}

	@Test
	public void testRemoveDepartment400() throws Exception {
		final String url = "/removeDepartment";
		mvc.perform(
				MockMvcRequestBuilders.get(url))
				.andExpect(status().isBadRequest());
	}
}
