angular.module("department-controller", [])
    .controller( "DepartmentsVM", function( $scope, $http ) {

        $scope.departments = [];
        $scope.numberOfEmptyDepartments = 0;

        var listAllDepartments = function() {
            $.get("/getAllDepartments", function( data ) {
                $scope.departments = [];

                data.forEach( function ( department ) {
                    $scope.departments.push( department );
                });
                
                getNumberOfEmptyDepartments();

                $scope.$apply();

            }).fail(function( data ) {
                alert(data.responseText);
            });
        }

        var getNumberOfEmptyDepartments = function() {
            $.get("/getEmptyDepartments", function(data){
                $scope.numberOfEmptyDepartments = data.length;
                $scope.$apply();

            }).fail(function( data ) {
                alert(data.responseText);
            });
        }

        $scope.remove = function(index) {
            var params = {id: $scope.departments[index].id};

            $.post("/removeDepartment", params, function() {
                listAllDepartments();
            });
        }

        listAllDepartments();

    });
