angular.module("employee-controller", [])
    .controller( "EmployeesVM", function( $scope, $http ) {

    $scope.employees = [];
    $scope.departments = [];

    $scope.newEmployeeName = "";
    $scope.newEmployeeDepartment = "";
    $scope.employeeEditId = "";

    $scope.employeeActionButton = "Adicionar";

    var listAllEmployess = function() {

        $scope.newEmployeeName = "";
        $scope.newEmployeeDepartment = "";
        $scope.employeeActionButton = "Adicionar";

        $.get("/getAllEmployees", function( data ) {
            $scope.employees = [];

            data.forEach( function( employee ) {
                $scope.employees.push( employee );
            });

            $scope.$apply();

        }).fail(function( data ) {
            alert(data.responseText);
        });
    }

    var listAllDepartments = function() {
        $.get("/getAllDepartments", function( data ) {
            $scope.departments = [];

            data.forEach( function ( department ) {
                $scope.departments.push( department );
            });

            $scope.$apply();

        }).fail(function( data ) {
            alert(data.responseText);
        });
    }


    $scope.takeAction = function() {
        if( $scope.employeeActionButton == 'Adicionar' ) {

            var params = {nome: $scope.newEmployeeName, departamento: $scope.newEmployeeDepartment};

            $.post("/saveEmployee", params, function(){
                listAllEmployess();

            }).fail(function( data ) {
                alert(data.responseText);
            });

        } else if( $scope.employeeActionButton == 'Confirmar' ) {

            var params = {
                    nome: $scope.newEmployeeName
                ,   departamento: $scope.newEmployeeDepartment
                ,   id: $scope.employeeEditId
            };

            $.post("/editEmployee", params, function(){
                listAllEmployess();

            }).fail(function( data ) {
                alert(data.responseText);
            });
        }
    }

    $scope.remove = function( index ) {
        var params = {id: $scope.employees[index].id};

        $.post( "/removeEmployee", params, function( data ){
            listAllEmployess();

        }).fail(function( data ) {
            alert(data.responseText);
        });
    }

    $scope.edit = function( index ) {
        $scope.employeeActionButton = "Confirmar";

        $scope.employeeEditId = $scope.employees[index].id;
        $scope.newEmployeeName = $scope.employees[index].name;
        $scope.newEmployeeDepartment = $scope.employees[index].department.id;

        $("#cancelButton").show();
    }

    $scope.cancel = function() {
        $scope.employeeActionButton = "Adicionar";
        $scope.newEmployeeName = "";
        $scope.newEmployeeDepartment = "";

        $("#cancelButton").hide();
    }

    listAllEmployess();
    listAllDepartments();

});
