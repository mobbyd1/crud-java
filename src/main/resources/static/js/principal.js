$(function () {
    $('.navbar-toggle').click(function () {
        $('.navbar-nav').toggleClass('slide-in');
        $('.side-body').toggleClass('body-slide-in');
        $('#search').removeClass('in').addClass('collapse').slideUp(200);
    });

    $('#descriptionPage').click(function() {
       $("#frame").attr("src", "description.html");
    });

    $('#employeesAnchor').click(function(){
        $("#frame").attr("src", "/employees");
    });

    $('#departamentsAnchor').click(function(){
        $("#frame").attr("src", "/departments");
    });
});

$.ajaxSetup({
    beforeSend: function () {
        $("#loader").show();
    },
    done: function () {
        $("#loader").hide();
    }
});