package com.prova.mobicare.controller;

import com.prova.mobicare.dlo.DepartmentDLO;
import com.prova.mobicare.dto.DepartmentDTO;
import com.prova.mobicare.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruhan on 17/09/17.
 */
@Controller
public class DepartmentController extends AbstractControllerWithErrorHandling {

    @Autowired
    DepartmentDLO departmentDLO;

    public DepartmentController() {
        super();
    }

    @RequestMapping("/departments")
    public String departments() {
        return "departments";
    }

    @RequestMapping("/getAllDepartments")
    public ResponseEntity getAllDepartments() throws Exception {
        final List<Department> departments = departmentDLO.getAll();
        return new ResponseEntity( getDepartmentDTOList(departments), HttpStatus.OK );
    }

    @RequestMapping("/removeDepartment")
    public ResponseEntity removeDepartment( @RequestParam Long id ) throws Exception {
        departmentDLO.removeDepartment(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping("/getEmptyDepartments")
    public ResponseEntity getEmptyDepartments() throws Exception {
        final List<Department> emptyDepartments = departmentDLO.getEmptyDepartments();
        return new ResponseEntity( getDepartmentDTOList(emptyDepartments), HttpStatus.OK );
    }

    private List<DepartmentDTO> getDepartmentDTOList(List<Department> departments ) {

        final List<DepartmentDTO> result = new ArrayList<>();

        for( Department department : departments ) {
            final Long id = department.getId();
            final String name = department.getName();
            final Integer numberOfEmployees = department.getEmployeeList().size();

            final DepartmentDTO dto = new DepartmentDTO();
            dto.setId( id.toString() );
            dto.setName( name );
            dto.setNumberOfEmployees( numberOfEmployees.toString() );

            result.add( dto );
        }

        return result;
    }
}
