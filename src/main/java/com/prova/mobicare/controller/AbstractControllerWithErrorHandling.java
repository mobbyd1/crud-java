package com.prova.mobicare.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by ruhandosreis on 18/09/17.
 */
public class AbstractControllerWithErrorHandling {

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity handleMissingParams(MissingServletRequestParameterException ex) {
        final String name = ex.getParameterName();
        final String msg = String.format("Preencha o %s", name);

        return new ResponseEntity(msg, HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException( Exception ex ) {
        return new ResponseEntity( "Ocorreu um erro inesperado", HttpStatus.INTERNAL_SERVER_ERROR );
    }

}
