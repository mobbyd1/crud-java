package com.prova.mobicare.controller;

import com.prova.mobicare.dlo.EmployeeDLO;
import com.prova.mobicare.dto.EmployeeDTO;
import com.prova.mobicare.model.Department;
import com.prova.mobicare.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruhan on 17/09/17.
 */
@Controller
public class EmployeeController extends AbstractControllerWithErrorHandling {

    @Autowired
    EmployeeDLO employeeDLO;

    public EmployeeController() {
        super();
    }

    @RequestMapping("/employees")
    public String employess() {
        return "employees";
    }

    @RequestMapping("/getAllEmployees")
    public ResponseEntity getAllEmployess() {
        final List<Employee> employees = employeeDLO.getAll();
        final List<EmployeeDTO> result = new ArrayList<>();

        for (Employee employee : employees) {
            final Long id = employee.getId();
            final String name = employee.getName();
            final String department = employee.getDepartment().getName();

            final EmployeeDTO employeeDTO = new EmployeeDTO();
            employeeDTO.setId( id.toString() );
            employeeDTO.setName( name );
            employeeDTO.setDepartment( department );

            result.add( employeeDTO );
        }

        return new ResponseEntity( result, HttpStatus.OK);
    }

    @RequestMapping("/saveEmployee")
    public ResponseEntity saveEmployee( @RequestParam String nome, @RequestParam Long departamento ) throws Exception {
        employeeDLO.saveEmployee( nome, departamento );
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping("/editEmployee")
    public ResponseEntity editEmployee(
                @RequestParam Long id
            ,   @RequestParam String nome
            ,   @RequestParam Long departamento ) throws Exception {

        employeeDLO.updateEmploye( id, nome, departamento );
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping("/removeEmployee")
    public ResponseEntity removeEmployee( @RequestParam Long id ) throws Exception {
        employeeDLO.removeEmployee( id );
        return new ResponseEntity(HttpStatus.OK);
    }
}
