package com.prova.mobicare.dao;

import com.prova.mobicare.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

/**
 * Created by ruhan on 17/09/17.
 */
public interface EmployeeDAO extends CrudRepository<Employee, Long> {

}
