package com.prova.mobicare.dao;

import com.prova.mobicare.model.Department;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by ruhan on 17/09/17.
 */
public interface DepartmentDAO extends CrudRepository<Department, Long> {

    @Query("SELECT d FROM Department d WHERE d.employeeList IS EMPTY")
    List<Department> getEmptyDepartments();
}
