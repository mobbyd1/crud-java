package com.prova.mobicare.dto;

/**
 * Created by ruhandosreis on 18/09/17.
 */
public class DepartmentDTO {

    private String id;
    private String name;
    private String numberOfEmployees;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(String numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
    }
}
