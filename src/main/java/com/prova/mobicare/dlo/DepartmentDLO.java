package com.prova.mobicare.dlo;

import com.prova.mobicare.dao.DepartmentDAO;
import com.prova.mobicare.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruhan on 17/09/17.
 */
@Component
public class DepartmentDLO {

    @Autowired
    private DepartmentDAO departmentDAO;

    @PostConstruct
    public void init() {
        final Department ti = new Department("TI");
        final Department rh = new Department("RH");
        final Department financeiro = new Department("Financeiro");
        final Department comercial = new Department("Comercial");

        saveDepartment( ti );
        saveDepartment( rh );
        saveDepartment( financeiro );
        saveDepartment( comercial );
    }

    public Department getById( final Long id ) {
        return departmentDAO.findOne( id );
    }

    public List<Department> getAll() {
        final List<Department> departments = new ArrayList<>();
        final Iterable<Department> all = departmentDAO.findAll();

        all.forEach( departments::add );
        return departments;
    }

    public void saveDepartment( Department department ) {
        departmentDAO.save( department );
    }

    public void removeDepartment( final Long id ) {
        final Department department = departmentDAO.findOne(id);
        departmentDAO.delete( department );
    }

    public List<Department> getEmptyDepartments() {
        return departmentDAO.getEmptyDepartments();
    }
}
