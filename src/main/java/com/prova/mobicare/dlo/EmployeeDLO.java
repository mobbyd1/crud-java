package com.prova.mobicare.dlo;

import com.prova.mobicare.dao.EmployeeDAO;
import com.prova.mobicare.model.Department;
import com.prova.mobicare.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruhan on 17/09/17.
 */
@Component
public class EmployeeDLO {

    @Autowired
    private EmployeeDAO employeeDAO;

    @Autowired
    private DepartmentDLO departmentDLO;

    public void saveEmployee( final String employeeName, final Long idDepartment ) {
        final Department department = departmentDLO.getById(idDepartment);

        final Employee employee = new Employee();
        employee.setName( employeeName );

        department.addEmployee( employee );
        departmentDLO.saveDepartment( department );
    }

    public void removeEmployee( final Long id ) {
        final Employee employee = employeeDAO.findOne(id);

        if( employee != null ) {
            employeeDAO.delete(employee);
        }
    }

    public void updateEmploye( final Long idEmployee, final String name, final Long idDepartment ) {
        final Employee employee = employeeDAO.findOne(idEmployee);
        final Department department = departmentDLO.getById(idDepartment);

        employee.setName( name );
        employee.setDepartment( department );

        employeeDAO.save( employee );
    }

    public List<Employee> getAll() {
        final List<Employee> employees = new ArrayList<>();
        final Iterable<Employee> all = employeeDAO.findAll();

        all.forEach( employees::add );
        return employees;
    }
}
