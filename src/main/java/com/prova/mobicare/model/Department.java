package com.prova.mobicare.model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruhan on 17/09/17.
 */
@Entity(name = "Department")
@Table(name = "department")
public class Department implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @OneToMany(
            mappedBy = "department",
            cascade = javax.persistence.CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Employee> employeeList = new ArrayList<>();

    public Department(){}

    public Department(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public void addEmployee( Employee e ) {
        this.employeeList.add( e );
        e.setDepartment( this );
    }

    @Override
    public String toString() {
        return String.format(
                "Department[id=%d, name='%s']",
                id, name );
    }
}
